#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <ctype.h>
#include <time.h>
#include <locale.h>
#include <unistd.h>
#include <fcntl.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <libgen.h>

#include <X11/Xlib.h>
#include <X11/Xatom.h>
#include <X11/Xutil.h>
#include <X11/Xft/Xft.h>

/* If it's good enough for Plan9 it's good enough for us */
#define uint   unsigned int

/* Technically an RFC made it only 4 bytes, but that just means that there are now
   6 byte programs in existence *and* 4 byte programs, so we should support it, sigh */
#define UTFmax 6

/* This is really the only global in the program, it's so we don't have to locally stack-allocate
   4096 bytes everywhere. */
#define MAXERRORLENGTH 4096
static char ErrorString[MAXERRORLENGTH] = {0};

enum {
	SCM_WINDOWBACK,
        SCM_WINDOWFORE,
        SCM_SELECTBACK,
        SCM_SELECTFORE,
	SCM_NUMITEMS
};

enum {
	WIN_TOFONTHEIGHT = -1,
	WIN_TOSCREENHEIGHT,
};

typedef struct {
	/* This is a 'nasty' solution to seeing if we need to free a structure is allocated or not.
	   _x is set if x is allocated and unset if it's not been allocated.
	   This problems arises because Xft returns a structure and not a pointer to structure
	 */

	int _winbg, _winfg, _selbg, _selfg;
	XftColor winbg, winfg;
	XftColor selbg, selfg;
} zhm_ColorScheme;

typedef struct {
	Display *display;
	int screen;
	XftDraw *draw;
	XftFont *font;
	Window rootwin, win;
	size_t width, height;
	size_t screenw, screenh;

	zhm_ColorScheme colorscheme;
	int key_isgrabbed;

	int _pixmap, _gc;
	Pixmap pixmap;
	GC gc;
} zhm_Display;

typedef struct {
	char *buffer;
	struct entry {
		char *chinese, *pinyin, *description;
	} *entries;
	size_t nentries;
} zhm_Table;

#define WORDLENGTH 32 
#define NMATCHES 256
#define MAXWORDS 256

typedef struct {
	char s[WORDLENGTH];
	ssize_t table_index;
	ssize_t pool_selection, pool_offset; /* So that, when we go back in the word list we can set these in a coherent way */
} zhm_Word;

typedef struct {
	XIM inputmethod;
	XIC incontext;

	size_t match_i, cursor; /* TODO: find a better name for match_i */
	char *inbuf; /* indexes match.matches[match_i].s; nothing more than a shortcut */
	struct {
		size_t start_xoff, pool_offset; /* offset used for drawing */
		size_t max_display_item; /* naming is difficult. TODO: Find a more consistent name */
		size_t leftarrow_width, rightarrow_width;

		/* Match pool stores potential matches */
		ssize_t pool[NMATCHES];
		size_t pool_i;
		ssize_t pool_selection;

		/* When the user has selected one, we shift it here and move on */
		zhm_Word matches[MAXWORDS];
	} match;
} zhm_Input;

#include "zhmenu.h"
#include "config.h"


/** Helper macros **/
/* I'm not particularly fond of using too many macros (code that uses them is generally Bad), but
   these are abstractions that are easy enough to read and comprehend, but do Great Good in eliminating
   the actual amount of code */

#define eprintf(...) fprintf(stderr, __VA_ARGS__)
#define THROW(...) { fprintf(stderr, __VA_ARGS__); goto _throw; }


/** Xft Wrapper Macros **/

#define WrapXftColorAllocName(window, colorname, xcolor)              \
  XftColorAllocName(window->display,                                  \
                    DefaultVisual(window->display, window->screen),   \
                    DefaultColormap(window->display, window->screen), \
                    colorname,                                        \
                    xcolor)

#define WrapXftColorFree(window, xcolor)                         \
  XftColorFree(window->display,                                  \
               DefaultVisual(window->display, window->screen),   \
               DefaultColormap(window->display, window->screen), \
               xcolor)


char *get_error_string(Display *display, int error_code)
{
	/* It returns int, but I cannot find documentation specifying what is returned.
	   So...  pretty much all we can do here is ignore it, unfortunately */
	XGetErrorText(display, error_code, ErrorString, MAXERRORLENGTH);

	return ErrorString; /* So we can treat the interface as a functional interface. :/ */
}


int col_load(zhm_Display *window, zhm_ColorScheme *scheme, char *schemetable[SCM_NUMITEMS])
{
	if (!window || !scheme || !schemetable
	 || !schemetable[SCM_WINDOWBACK] || !schemetable[SCM_WINDOWFORE]
	 || !schemetable[SCM_SELECTBACK] || !schemetable[SCM_SELECTFORE])
	{
		eprintf("col_load: important input value is null\n");
		return -1;
	}

	if (!(WrapXftColorAllocName(window, schemetable[SCM_WINDOWBACK], &(scheme->winbg)))) {
		THROW("Error while allocating window background\n");
	}
	scheme->_winbg = 1;
	if (!(WrapXftColorAllocName(window, schemetable[SCM_WINDOWFORE], &(scheme->winfg)))) {
		THROW("Error while allocating window foreground\n");
	}
	scheme->_winfg = 1;
	if (!(WrapXftColorAllocName(window, schemetable[SCM_SELECTBACK], &(scheme->selbg)))) {
		THROW("Error while allocating selection background\n");
	}
	scheme->_selbg = 1;
	if (!(WrapXftColorAllocName(window, schemetable[SCM_SELECTFORE], &(scheme->selfg)))) {
		THROW("Error while allocating selection foreground\n");
	}
	scheme->_selfg = 1;
	return 0;

_throw:
	col_unload(window, scheme);
	return -1;
}


void col_unload(zhm_Display *window, zhm_ColorScheme *scheme)
{
	if (!window || !window->display || !scheme) { eprintf("col_unload: input value is null\n"); return; }
	if (scheme->_winbg) { WrapXftColorFree(window, &(scheme->winbg)); scheme->_winbg = 0; }
	if (scheme->_winfg) { WrapXftColorFree(window, &(scheme->winfg)); scheme->_winfg = 0; }
	if (scheme->_selbg) { WrapXftColorFree(window, &(scheme->selbg)); scheme->_selbg = 0; }
	if (scheme->_selfg) { WrapXftColorFree(window, &(scheme->selfg)); scheme->_selfg = 0; }
}


int win_open(zhm_Display *window, int x, int y, size_t width, ssize_t height, char *schemetable[SCM_NUMITEMS], int force_position, char *font)
{
	XWindowAttributes wattr = {0};
	if (!window || !schemetable || !font) { eprintf("win_open: input value is null\n"); return -1; }

	if (!(window->display = XOpenDisplay(NULL))) { goto _throw; }
	window->screen = DefaultScreen(window->display);
	window->rootwin = RootWindow(window->display, window->screen);
	
	memset((void*)&(window->colorscheme), 0, sizeof(zhm_ColorScheme));
	if ((col_load(window, &(window->colorscheme), schemetable)) < 0) {
		unsigned long white, black;

		eprintf("%s\n", "Falling back to backup colorscheme!");

		/* This is some pretty 'dark magic' in this context, I guess... */
		white = WhitePixel(window->display, window->screen);
		black = BlackPixel(window->display, window->screen);
		window->colorscheme.winfg.pixel = window->colorscheme.selbg.pixel = white;
		window->colorscheme.winbg.pixel = window->colorscheme.selfg.pixel = black;
	}

	if (!(window->font = XftFontOpenName(window->display, window->screen, font))) {
		THROW("Error in XftFontOpenName with font string: '%s'\n", font);
	}

	window->height = (height == WIN_TOFONTHEIGHT)   ? window->font->height 
	               : (height == WIN_TOSCREENHEIGHT) ? XHeightOfScreen(ScreenOfDisplay(
		                                                             window->display,
									     window->screen))
		       : (size_t)height;

	window->width = !width ? XWidthOfScreen(ScreenOfDisplay(window->display, window->screen))
	                       : width;

	/* x and y position, along with border width, are almost always overwritten by the WM */
	if (!(window->win = XCreateSimpleWindow(window->display,
	                                        window->rootwin,
	                                        x, y,
	                                        window->width, window->height,
	                                        2,
	                                        window->colorscheme.winbg.pixel,
	                                        window->colorscheme.winbg.pixel))) {
		THROW("Error at XCreateSimpleWindow\n");
	}

	if (!(XChangeWindowAttributes(window->display, window->win, CWEventMask | CWOverrideRedirect,
	                              &((XSetWindowAttributes) {
		.event_mask = VisibilityChangeMask
		            | LeaveWindowMask
		            | EnterWindowMask
		            | KeyPressMask
		            | KeyReleaseMask
		            | ExposureMask,
		.override_redirect = force_position
	})))) {
		THROW("Error at XChangeWindowAttributes\n");
	}

	if (!XGetWindowAttributes(window->display, window->win, &wattr)) {
		THROW("XGetWindowAttributes returned critical failure\n");
	}
	window->width = wattr.width;
	window->height = wattr.height;

	window->pixmap = XCreatePixmap(window->display, window->rootwin,
	                               window->width, window->height,
	                               DefaultDepth(window->display, window->screen));
	window->_pixmap = 1;

	window->gc = XCreateGC(window->display, window->rootwin, 0, NULL);
	window->_gc = 1;

	if (!(XSetLineAttributes(window->display, window->gc, 1, LineSolid, CapButt, JoinMiter))) {
		THROW("Error at XSetLineAttributes\n");
	}

	if (!(window->draw = XftDrawCreate(window->display, window->pixmap,
	                                 DefaultVisual(window->display, window->screen),
	                                 DefaultColormap(window->display, window->screen)))) {
		THROW("Error in XftDrawCreate\n");
	}

	if (!XMapRaised(window->display, window->win)) {
		THROW("Error at XMapRaised\n");
	}
	if (!XSync(window->display, True)) {
		THROW("Error at XSync\n");
	}

	return 0;
_throw:
	win_close(window);
	return -1;
}


void win_close(zhm_Display *window)
{
	if (!window) { eprintf("win_close: input value is null\n"); return; }
	col_unload(window, &(window->colorscheme));
	if (window->_gc) {
		if (!XFreeGC(window->display, window->gc)) {
			eprintf("%s\n", "XFreeGC: Bad GC??");
		}
		window->_gc = 0;
	}
	if (window->_pixmap) {
		if (!XFreePixmap(window->display, window->pixmap)) {
			eprintf("%s\n", "XFreePixmap: Bad pixmap??");
		}
		window->_pixmap = 0;
	}
	if (window->win) {
		if (!(XDestroyWindow(window->display, window->win))) {
			eprintf("%s\n", "XDestroyWindow: Bad window??");
		}
		window->win = 0;
	}
	if (window->draw)    { XftDrawDestroy(window->draw); window->draw = NULL; }
	if (window->display) { XCloseDisplay(window->display); window->display = NULL; }
}


int win_drawrect(zhm_Display *window, XftColor *color, int x, int y, uint w, uint h)
{
	if (!color) { eprintf("win_drawrect: input value is null\n"); return -1; }
	if (!XSetForeground(window->display, window->gc, color->pixel)) {
		THROW("Error in XSetForeground\n");
	}
	if (!XFillRectangle(window->display, window->pixmap, window->gc, x, y, w, h)) {
		THROW("Error in XSetForeground\n");
	}
	XftDrawRect(window->draw, color, x, y, w, h);
	return 0;
_throw:
	return -1;
}


int win_drawstring(zhm_Display *window, XftColor *color, char *s, size_t l, int x, int y)
{
	size_t windowh, fonth, drawy;
	if (!window || !s) { eprintf("win_drawstring: input value is null\n"); return -1; }

	/* The underlying formula is blatantly snarfed from Dmenu. It results in nice centered text */
	windowh = window->font->height;
	fonth = window->font->ascent+window->font->descent;
	drawy = y + (windowh - fonth) / 2 + window->font->ascent;

	XftDrawStringUtf8(window->draw, color, window->font, x, drawy, (const FcChar8 *)s, l);
	return 0;
}


ssize_t win_drawarrow(zhm_Display *window, zhm_Input *input, XftColor *fg, XftColor *bg, size_t x, char *arrow)
{
	if (!window || !input || !fg || !bg || !arrow) { eprintf("win_drawarrow: input value is null\n"); return -1; }

	if ((win_drawstring(window, fg, arrow, strlen(arrow), x, 0)) < 0) {
		return -1;
	}

	return 0;
}


/* We return the width of the item so we can get a cumulative offset */
ssize_t win_drawitem(zhm_Display *window, zhm_Input *input, XftColor *fg, XftColor *bg, int selected, size_t x,
                     struct entry *e)
{
	XGlyphInfo extents = {0};

	if (!window || !input || !fg || !bg || !e) { eprintf("win_drawitem: input value is null\n"); return -1; }

	XftTextExtentsUtf8(window->display, window->font, (const FcChar8 *)e->chinese,
	                   strlen(e->chinese), &extents);

	/* Ideally this logic would be external to the function but, to be honest it's a lot
	 * more scaffolding for very little gain. It might be worth abstracting at some point */
	if (selected) {
		if ((win_drawrect(window, bg, x, 0, extents.width, window->font->height)) < 0) {
			return -1;
		}
	}

	if ((win_drawstring(window, fg, e->chinese, strlen(e->chinese), x, 0)) < 0) {
		return -1;
	}

	return extents.width; /* We could, apparently, also use extents.yOff #TheMoreYouKnow */
}


int win_blit(zhm_Display *window)
{
	if (!window) { eprintf("win_blit: input value null\n"); return -1; }
	if (!XCopyArea(window->display, window->pixmap, window->win, window->gc,
	               0, 0, window->width, window->height, 0, 0)) {
		THROW("XCopyArea returned critical failure\n");
	}
	if (!XSync(window->display, False)) {
		THROW("Error at XSync\n");
	}
	return 0;
_throw:
	return -1;
}


int win_clear(zhm_Display *window)
{
	if (!window) { eprintf("win_clear: input value null\n"); return -1; }
	if (!XClearWindow(window->display, window->win)) {
		THROW("XClearWindow returned critical failure\n");
	}
	if (!XSync(window->display, False)) {
		THROW("Error at XSync\n");
	}
	return 0;
_throw:
	return -1;
}


int win_raise(zhm_Display *window)
{
	if (!window) { eprintf("win_raise: input value null\n"); return -1; }
	if (!XRaiseWindow(window->display, window->win)) {
		THROW("XRaiseWindow returned critical failure\n");
	}
	return 0;

_throw:
	return -1;
}


int win_resize(zhm_Display *window, int width, int height)
{
	if (!window) { eprintf("win_resize: input value null\n"); return -1; }
	if (!width)  { width = window->width; }
	if (!height) { height = window->height; }

	if (!XResizeWindow(window->display, window->win, width, height)) {
		THROW("XResizeWindow returned critical failure\n");
	}
	window->width = width;
	window->height = height;
	return 0;

_throw:
	return -1;
}


const char *ettoa(int event_type)
{
	char *s = NULL;
	if (event_type == KeyPress)		{ s = "KeyPress"; }
	if (event_type == KeyRelease)		{ s = "KeyRelease"; }
	if (event_type == ButtonPress)		{ s = "ButtonPress"; }
	if (event_type == ButtonRelease)	{ s = "ButtonRelease"; }
	if (event_type == MotionNotify)		{ s = "MotionNotify"; }
	if (event_type == EnterNotify)		{ s = "EnterNotify"; }
	if (event_type == LeaveNotify)		{ s = "LeaveNotify"; }
	if (event_type == FocusIn)		{ s = "FocusIn"; }
	if (event_type == FocusOut)		{ s = "FocusOut"; }
	if (event_type == KeymapNotify)		{ s = "KeymapNotify"; }
	if (event_type == Expose)		{ s = "Expose"; }
	if (event_type == GraphicsExpose)	{ s = "GraphicsExpose"; }
	if (event_type == NoExpose)		{ s = "NoExpose"; }
	if (event_type == CirculateRequest)	{ s = "CirculateRequest"; }
	if (event_type == ConfigureRequest)	{ s = "ConfigureRequest"; }
	if (event_type == MapRequest)		{ s = "MapRequest"; }
	if (event_type == ResizeRequest)	{ s = "ResizeRequest"; }
	if (event_type == CirculateNotify)	{ s = "CirculateNotify"; }
	if (event_type == ConfigureNotify)	{ s = "ConfigureNotify"; }
	if (event_type == CreateNotify)		{ s = "CreateNotify"; }
	if (event_type == DestroyNotify)	{ s = "DestroyNotify"; }
	if (event_type == GravityNotify)	{ s = "GravityNotify"; }
	if (event_type == MapNotify)		{ s = "MapNotify"; }
	if (event_type == MappingNotify)	{ s = "MappingNotify"; }
	if (event_type == ReparentNotify)	{ s = "ReparentNotify"; }
	if (event_type == UnmapNotify)		{ s = "UnmapNotify"; }
	if (event_type == VisibilityNotify)	{ s = "VisibilityNotify"; }
	if (event_type == ColormapNotify)	{ s = "ColormapNotify"; }
	if (event_type == ClientMessage)	{ s = "ClientMessage"; }
	if (event_type == PropertyNotify)	{ s = "PropertyNotify"; }
	if (event_type == SelectionClear)	{ s = "SelectionClear"; }
	if (event_type == SelectionNotify)	{ s = "SelectionNotify"; }
	if (event_type == SelectionRequest)	{ s = "SelectionRequest"; }
	return s;
}


int key_grab(zhm_Display *window)
{
	int status = 0;
	struct timespec millisecond = { .tv_sec = 0, .tv_nsec = 1000000 };
	struct timespec left = { 0 };

	if (!window) { eprintf("key_grab: input value null\n"); return -1; }

	/* Grab Keyboard */
	while ((status = XGrabKeyboard(window->display, window->rootwin, True, GrabModeAsync,
	                               GrabModeAsync, CurrentTime)) != GrabSuccess) {
		if (status == BadWindow || status == BadValue) {
			THROW("XGrabKeyboard returned critical failure\n");
		}
		left = millisecond;
		while ((nanosleep(&left, &left)) < 0)
			;
	}
	window->key_isgrabbed = 1;
	return 0;
_throw:
	return -1;
}


void key_ungrab(zhm_Display *window)
{
	if (!window || !window->display) { eprintf("key_ungrab: input value null\n"); return; }
	if (window->key_isgrabbed) {
		XUngrabKeyboard(window->display, CurrentTime);
		window->key_isgrabbed = 0;
	}
}


int key_open(zhm_Display *window, zhm_Input *input, zhm_Table *table)
{
	size_t i;
	char s[WORDLENGTH+1] = {0};
	XGlyphInfo extents = {0};
	if (!window || !input) { eprintf("key_open: input value null\n"); return -1; }

	if (!(input->inputmethod = XOpenIM(window->display, NULL, NULL, NULL))) {
		THROW("XOpenIM: Cannot open input device\n");
	}
	if (!(input->incontext = XCreateIC(input->inputmethod, XNInputStyle,
	                                   XIMPreeditNothing | XIMStatusNothing, XNClientWindow,
					   window->win, XNFocusWindow, window->win, NULL))) {
		THROW("XCreateIC: Cannot create input context\n");
	}

	input->match_i = input->cursor = 0;
	input->inbuf = input->match.matches[0].s;

	/* Grab a rough approximation of the input buffer length so we won't draw overlapping it */
	memset(s, '_', WORDLENGTH);
	XftTextExtentsUtf8(window->display, window->font, (const FcChar8 *)s, WORDLENGTH, &extents);
	input->match.start_xoff = extents.width + WORDLENGTH;

	/* Grab the width of the left and right arrows to draw when things go off-screen */
	XftTextExtentsUtf8(window->display, window->font,
	                   (const FcChar8 *)item_leftarrow, strlen(item_leftarrow), &extents);
	input->match.leftarrow_width = extents.width;
	XftTextExtentsUtf8(window->display, window->font,
	                   (const FcChar8 *)item_rightarrow, strlen(item_rightarrow), &extents);
	input->match.rightarrow_width = extents.width;

	for (i = 0; i < NMATCHES; i++) {
		input->match.pool[i] = -1;
	}
	input->match.pool_i = input->match.pool_offset = 0;
	input->match.max_display_item = 0;
	input->match.pool_selection = -1;

	for (i = 0; i < MAXWORDS; i++) {
		memset(input->match.matches[i].s, (char)'\0', WORDLENGTH);
		input->match.matches[i].table_index = -1;
	}

	return 0;

_throw:
	return -1;
}


void key_close(zhm_Input *input)
{
	if (!input) { return; }
	if (input->incontext) { XDestroyIC(input->incontext); }
	if (input->inputmethod) { XCloseIM(input->inputmethod); }
}


int key_addgrapheme(zhm_Input *input, char grapheme[UTFmax])
{
	size_t sz = strlen(grapheme);
	if (!input || !grapheme) { eprintf("key_addgrapheme: input value null\n"); return -1; }
	if (strlen(input->inbuf) + sz > WORDLENGTH-1) { return 0; }

	if (input->inbuf[input->cursor] == '\0') {
		memcpy(input->inbuf+input->cursor, grapheme, sz);
		input->cursor += sz;
	}
	else {
		char *rest = input->inbuf+input->cursor;
		memmove(rest+sz, rest, strlen(rest));
		memcpy(rest, grapheme, sz);
		input->cursor += sz;
	}

	return 0;
}


void key_delete(zhm_Input *input)
{
	char *s = NULL;
	if (!input) { eprintf("key_press: input value null\n"); return; }
	if (input->cursor == WORDLENGTH-1) { return; }
	s = input->inbuf + input->cursor;
	memmove(s, s+1, strlen(s));
}


void key_rubout(zhm_Input *input)
{
	if (!input) { eprintf("key_press: input value null\n"); return; }
	if (input->cursor > 0) {
		input->inbuf[input->cursor-1] = '\0';
		key_prevchar(input);
	}
	else if (input->cursor == 0 && input->match_i > 0) {
		/* clear the current selection entry*/
		memset(input->match.matches[input->match_i].s, '\0', WORDLENGTH);
		input->match.matches[input->match_i].table_index = -1;
		input->match.matches[input->match_i].pool_selection = -1;
		input->match.matches[input->match_i].pool_offset = 0;

		/* go backwards */
		input->match_i--;

		/* load gui values from previous selection entry */
		input->inbuf = input->match.matches[input->match_i].s;
		input->match.pool_selection = input->match.matches[input->match_i].pool_selection;
		input->match.pool_offset = input->match.matches[input->match_i].pool_offset;

		/* place cursor at the end of the input buffer */
		input->cursor = strlen(input->inbuf);
	}
}


void key_prevchar(zhm_Input *input)
{
	if (!input) { eprintf("key_press: input value null\n"); return; }
	input->cursor = (input->cursor == 0) ? 0 : input->cursor-1;
}


void key_nextchar(zhm_Input *input)
{
	if (!input) { eprintf("key_press: input value null\n"); return; }
	input->cursor = (input->inbuf[input->cursor] == '\0') ? input->cursor : input->cursor+1;
}


void match_next(zhm_Input *input)
{
	if (!input) { eprintf("match_next: input value null\n"); return; }
	if (input->match.pool_selection+1 >= NMATCHES) { return; }
	input->match.pool_selection++;
	/* We rely on the fact that we draw before we check input, to initialize match.max_display_item
	 * This is messy but the current design kind of backs us into this corner. If we refactor in the future,
	 * it's probably worth tagging the entire table with the font extent width, so we can calculate/predict overflow 
	 * and the like in advance where we need that information, rather than relying on the actual drawing logic to
	 * give us this information... Hindsight is 2020 */
	if (input->match.pool_selection > input->match.max_display_item) {
		/* this should probably only ever be 1 ?? */
		size_t delta = abs(input->match.pool_selection - input->match.max_display_item);
		input->match.pool_offset += delta;
	}
}


struct entry *match_poolindex(zhm_Input *input, zhm_Table *table, size_t i)
{
	ssize_t j = 0;
	if ((j = input->match.pool[i]) < 0) { return NULL; }
	return &(table->entries[j]);
}


void match_prev(zhm_Input *input)
{
	if (!input) { eprintf("match_prev: input value null\n"); return; }
	if (input->match.pool_selection-1 >= -1) { input->match.pool_selection--; }
	if (input->match.pool_selection < input->match.pool_offset) {
		input->match.pool_offset = input->match.pool_selection;
	}
}


void match_gostart(zhm_Input *input)
{
	input->match.pool_selection = -1;
	input->match.pool_offset = 0;
}


void match_select(zhm_Input *input, zhm_Table *table)
{
	if (!input || !table) { eprintf("match_prev: input value null\n"); return; }

	if (input->match.pool_selection == -1) { return; }

	/* Add selection to list */
	input->match.matches[input->match_i].table_index = input->match.pool[input->match.pool_selection];
	input->match.matches[input->match_i].pool_selection = input->match.pool_selection;
	input->match.matches[input->match_i].pool_offset = input->match.pool_offset;

	match_gostart(input);

	/* Set to next match item */
	if (input->match_i < MAXWORDS) {
		input->match_i++;
		input->inbuf = input->match.matches[input->match_i].s;
		input->cursor = 0;
	}
	else {
		eprintf("match_select: Looks like the user filled the word quota?\n"
		        "You may wish to consider increasing MAXWORDS at compile time\n"
			"But yeah, there's basically nothing that we can do about this.\n");

	}

	/* TODO: Set match.is_populated = 0 */

	/* Clean out match pool! */
}


void match_populate(zhm_Input *input, zhm_Table *table)
{
	size_t i = 0;
	size_t j = 0;
	
	if (strlen(input->inbuf) == 0) {
		// printf("Regenerating list... [With defaults]\n");
		for (i = j = 0; i < NMATCHES && j < table->nentries; j++, i++) {
			input->match.pool[i] = j;
		}
		return;
	}

	/* TODO: */
	/* Check for match.is_populated = 0 */
	/* If 1, then we only have to _refine_, not repopulate, except for a deletion */
	/* Sidenote: Might not be viable if match pool size is smaller than table size */

	/* printf("selection: %ld\n", input->match.pool_selection);
	   printf("Regenerating list... (Word: '%s')\n", input->inbuf); */
	for (i = j = 0; i < NMATCHES && j < table->nentries; j++) {
		if (pinyincmp(table->entries[j].pinyin, input->inbuf)) {
			input->match.pool[i] = j;
			input->match.pool_i = i++;
		}
	}
}


void match_output(zhm_Input *input, zhm_Table *table)
{
	size_t i = 0;
	for (i = 0; i < MAXWORDS && input->match.matches[i].table_index > -1; i++) {
		printf("%s", (table->entries[input->match.matches[i].table_index]).chinese);
	}
}


/* This is where we use pool_offset, I think */
void match_draw(zhm_Display *window, zhm_Input *input, zhm_Table *table)
{
	XftColor *fg, *bg;
	struct entry *e = NULL;
	ssize_t xoff;
	int i;

	if (!window || !input || !table) { eprintf("match_draw: input value null\n"); return; }

	xoff = input->match.start_xoff;

	if (input->match.pool_offset > 0) {
		fg = &(window->colorscheme.winfg);
		bg = &(window->colorscheme.winbg);
		if ((win_drawarrow(window, input, fg, bg, xoff, item_leftarrow)) < 0) { return; }
		xoff += input->match.leftarrow_width + item_padwidth;
	}

	for (i = input->match.pool_offset; i < NMATCHES; i++)
	{
		ssize_t width;
		int selection;
		if (!(e = match_poolindex(input, table, i))) { break; }

		selection = (i == input->match.pool_selection);
		fg = selection ? &(window->colorscheme.selfg) : &(window->colorscheme.winfg);
		bg = selection ? &(window->colorscheme.selbg) : &(window->colorscheme.winbg);
		
		if ((width = win_drawitem(window, input, fg, bg, selection, xoff, e)) < 0) { return; }
		xoff += width + item_padwidth;

		/* Chinese characters are _mostly_ the same length so we can make an educated guess.
		 * There is potential for this to break, though. Especially if we're repurposing zhmenu
		 * for something like an emoji menu or something else */
		if (i+1 < NMATCHES && xoff + (width + item_padwidth) > window->width - width) {
			if ((win_drawarrow(window, input, fg, bg, xoff, item_rightarrow)) < 0) { return; }
			input->match.max_display_item = i;
			break;
		}
	}

	return;
}


int pinyincmp(char *pinyin, char *string)
{
	return pinyin && string && strstr(pinyin, string) == pinyin;
}


int key_press(zhm_Input *input, zhm_Table *table, XKeyEvent *keyevent)
{
	int status = 0;
	char grapheme[UTFmax] = { 0 };
	KeySym key;

	if (!input || !keyevent) { eprintf("key_press: input value null\n"); return -1; }

	Xutf8LookupString(input->incontext, keyevent, grapheme, UTFmax, &key, &status);

	if (status == XLookupKeySym || status == XLookupBoth || status == XLookupChars)
	{
		if (key == XK_Escape)         { goto _die; } /* the goto actually improves readability imo */
		else if (key == XK_Delete)    { key_delete(input); }
		else if (key == XK_BackSpace) { key_rubout(input); }
		else if (key == XK_Left)      { match_prev(input); }
		else if (key == XK_Right)     { match_next(input); }
		else if (key == XK_space)     { match_select(input, table); }
		else if (key == XK_Return)    { match_select(input, table); 
			                        match_output(input, table); goto _die; }
		else if (key == XK_Home)      { match_gostart(input); }
		else {
			if (status == XLookupBoth || status == XLookupChars) {
				if ((key_addgrapheme(input, grapheme)) < 0) { goto _throw; }
			}
		}
		match_populate(input, table);
	}
	else if (status == XLookupNone) {
		;
	}
	else {
		eprintf("Xutf8LookupString error\n");
	}

	return 1;
_die:
	return 0;
_throw:
	return -1;
}


int win_drawstate(zhm_Display *window, zhm_Input *input, zhm_Table *table)
{
	XftColor *fg, *bg;
	XGlyphInfo extents = {0};

	/* "sensible defaults" */
	size_t cursor_x = 1;
	size_t cursor_w = 3;
	size_t cursor_h;
	size_t cursor_wpad = 2;
	size_t restlen = 0;

	if (!window || !input) { eprintf("win_drawstate: input value null\n"); return -1; }

	fg = &(window->colorscheme.winfg);
	bg = &(window->colorscheme.winbg);

	/* draw background */
	if ((win_drawrect(window, bg, 0, 0, window->width, window->height))) { goto _throw; }

	if (input->cursor) {
		/* draw start of text up to cursor */
		if ((win_drawstring(window, fg, input->inbuf, input->cursor, 1, 1)) < 0) {
			goto _throw;
		}
		XftTextExtentsUtf8(window->display, window->font, (const FcChar8 *)input->inbuf,
		                   input->cursor, &extents);
	}

	cursor_x = extents.width + cursor_wpad;
	cursor_h = window->font->ascent+window->font->descent;
	if ((win_drawrect(window, fg, cursor_x, 0, cursor_w, cursor_h)) < 0) { goto _throw; }

	restlen = strlen(input->inbuf+input->cursor);
	if (restlen) {
		if ((win_drawstring(window, fg, input->inbuf + input->cursor, restlen,
				    cursor_x+cursor_w+cursor_wpad, 0)) < 0) {
			goto _throw;
		}
	}

	match_draw(window, input, table);

	win_blit(window);

	return 0;

_throw:
	return -1;
}


size_t file_lines(char *buffer)
{
	size_t i = 0;
	if (!buffer) { eprintf("file_lines: input value null\n"); return 0; }
	for (; *buffer; buffer++) {
		i += (*buffer == '\n');
	}
	return i;
}


char *file_buffer(char *path)
{
	char *buffer = NULL;
	int fd = -1;
	size_t size = 0;
	struct stat st = {0};

	if (!path) { eprintf("file_buffer: input value null\n"); return NULL; }
	if ((stat(path, &st)) < 0) { return NULL; }
	if (st.st_size == 0) { return NULL; }
	size = st.st_size;

	if (!(buffer = calloc(size+1, 1))) { return NULL; }
	if ((fd = open(path, O_RDONLY)) < 0) { goto _throw; }
	if ((read(fd, buffer, size)) < 0) { goto _throw; }
	if ((close(fd)) < 0) { perror("close"); }

	return buffer;

_throw:
	if (fd > 0) { close(fd); }
	if (buffer) { free(buffer); }
	return NULL;
} 


char *table_path(char *paths)
{
	size_t len = 0;
	char *start, *end;
	char *rs = NULL;

	if (!paths) { eprintf("table_path: input value null\n"); return NULL; }

	start = paths;
	while (*start) {
		/* Grab length and end of path */
		for (end = start, len = 0; *end && *end != ':'; end++, len++)
			;

		if (*end == ':') { end++; }

		/* Grab path */
		if (!(rs = calloc(len+1, 1))) { return NULL; }
		memcpy(rs, start, len);
		rs[len] = '\0';

		/* Test existence of file */
		if ((access(rs, R_OK)) >= 0) {
			break;
		}

		/* Test failed, try next path */
		free(rs); rs = NULL;
		start = end;
	}
	if (!rs) { eprintf("No readable database files\n"); }
	return rs;
}


int table_open(zhm_Table *t, char *paths)
{
	char *path = NULL;
	char *s = NULL;
	size_t i = 0;
	if (!t || !paths) { eprintf("table_load: input value null\n"); return -1; }
	if (!(path = table_path(paths))) { return -1; }
	if (!(t->buffer = file_buffer(path))) { goto _throw; }
	free(path); path = NULL;
	if (!(t->nentries = file_lines(t->buffer))) { goto _throw; }
	if (!(t->entries = calloc(t->nentries, sizeof(struct entry)))) { goto _throw; }

	for (s = t->buffer, i = 0; *s && i < t->nentries; i++) {
		t->entries[i].chinese = s;
		while (*s && *s != '\t')
			s++;
		if (!*s) { eprintf("Stopped parsing chinese early at line %zu\n", i); break; }
		*s++ = '\0';

		t->entries[i].pinyin = s;
		while (*s && *s != '\t')
			s++;
		if (!*s) { eprintf("Stopped parsing pinyin early at line %zu\n", i); break; }
		*s++ = '\0';

		t->entries[i].description = s;
		while (*s && *s != '\n')
			s++;
		if (!*s) { eprintf("Stopped parsing description early at line %zu\n", i); break; }
		*s++ = '\0';
	}
	if (i > t->nentries) { THROW("table_load: file exceeds table bounds\n"); }
	return 0;

_throw:
	if (path) { free(path); }
	if (t->entries) { free(t->entries); t->entries = NULL; }
	if (t->buffer) { free(t->buffer); t->buffer = NULL; }
	return -1;
}


void table_close(zhm_Table *t)
{
	if (!t) { eprintf("table_close: input value null\n"); return; }
	if (t->entries) { free(t->entries); t->entries = NULL; }
	if (t->buffer) { free(t->buffer); t->buffer = NULL; }
}


void help(char *progname, size_t width, size_t x, size_t y)
{
	/* Grab the width so we can display a default value. Ideally this would be abstracted,
	 * but it's really a one-shot thing */
	if (!width)
	{
		Display *tmpdisp = NULL;
		if ((tmpdisp = XOpenDisplay(NULL))) {
			size_t tmpscreen = DefaultScreen(tmpdisp);
			width = XWidthOfScreen(ScreenOfDisplay(tmpdisp, tmpscreen));
			XCloseDisplay(tmpdisp);
		}
	}

	printf("%s [OPTIONS]  --  adaptable chinese-to-pinyin input menu\n"
	       "OPTIONS:\n"
	       "    -h            --  Display this help string\n"
	       "    -w <number>   --  Set window width (Current: %zu)\n"
	       "    -x <number>   --  Set window x position (Current: %zu)\n"
	       "    -y <number>   --  Set window y position (Current: %zu)\n"
	       "    -f <string>   --  Set font, accepts an Xft font string (Current: '%s')\n"
	       "    -db <string>  --  Use alternate database search string (Current: '%s')\n"
	       "\n"
	       "See zhmenu(1) for more info",
	       basename(progname), width, x, y, fontstr, dbpath);
}


ssize_t atolu(char *s)
{
	ssize_t n = 0;
	if (!s) { eprintf("atolu: input value null\n"); return -1; }
	while (*s && isdigit(*s)) {
		n *= 10;
		n += (*s - '0');
		s++;
	}
	if (*s && !isdigit(*s)) { return -1; }
	return n;
}


int parse_arguments(char **argv, size_t *width, size_t *x, size_t *y, char **fontstr, char **dbpath)
{
	if (!argv || !width || !x || !y || !fontstr || !dbpath) { eprintf("parse_string: input value null\n"); return -1; }
	if (*argv) { argv++; } /* Skip program name */
	while (*argv) {
		if ((strcmp(*argv, "-w")) == 0) {
			ssize_t n = 0;
			if (!argv[1]) { eprintf("Missing argument to flag '-w'\n"); return -1; }
			if ((n = atolu(argv[1])) < 0) { eprintf("Bad number: '%s'\n", argv[1]); return -1; }
			*width = n;
			argv += 2;
		}
		else if ((strcmp(*argv, "-x")) == 0) {
			ssize_t n = 0;
			if (!argv[1]) { eprintf("Missing argument to flag '-x'\n"); return -1; }
			if ((n = atolu(argv[1])) < 0) { eprintf("Bad number: '%s'\n", argv[1]); return -1; }
			*x = n;
			argv += 2;
		}
		else if ((strcmp(*argv, "-y")) == 0) {
			ssize_t n = 0;
			if (!argv[1]) { eprintf("Missing argument to flag '-y'\n"); return -1; }
			if ((n = atolu(argv[1])) < 0) { eprintf("Bad number: '%s'\n", argv[1]); return -1; }
			*y = n;
			argv += 2;
		}
		else if ((strcmp(*argv, "-f")) == 0) {
			if (!argv[1]) { eprintf("Missing argument to flag '-f'\n"); return -1; }
			*fontstr = argv[1];
			argv += 2;
		}
		else if ((strcmp(*argv, "-db")) == 0) {
			if (!argv[1]) { eprintf("Missing argument to flag '-db'\n"); return -1; }
			*dbpath = argv[1];
			argv += 2;
		}
		else if ((strcmp(*argv, "-h")) == 0) {
			return -1;
		}
		else {
			eprintf("Unknown flag '%s'\n", *argv);
			return -1;
		}
	}
	return 1;
}


int main(int argc, char **argv)
{
	zhm_Display window = {NULL};
	zhm_Input input = {NULL};
	zhm_Table table = {NULL};
	XEvent event;

	size_t width, x, y;
	char *fontstring = fontstr;
	char *datapath = dbpath;
	width = x = y = 0;

	if (!setlocale(LC_CTYPE, "") || !XSupportsLocale()) { eprintf("Unsupported Locale\n"); }

	if (argc > 0) {
		if ((parse_arguments(argv, &width, &x, &y, &fontstring, &datapath)) < 0) {
			help(argv[0], width, x, y);
			exit(1);
		}
	}

	if ((table_open(&table, datapath)) < 0) {
		goto _throw;
	}
	if ((win_open(&window, x, y, width, WIN_TOFONTHEIGHT, colorscheme, 1, fontstring)) < 0) {
		goto _throw;
	}
	if ((key_open(&window, &input, &table)) < 0) {
		goto _throw;
	}
	
	match_populate(&input, &table);
	if ((win_drawstate(&window, &input, &table)) < 0) { goto _throw; }
	if ((win_blit(&window)) < 0) { goto _throw; }

	if ((key_grab(&window)) < 0) { goto _throw; }

	while (!XNextEvent(window.display, &event)) {
		if (XFilterEvent(&event, window.win)) {
			key_ungrab(&window);
			continue;
		}
		else if (event.type == DestroyNotify) {
			if (event.xdestroywindow.window == window.win) { break; }
		}
		else if (event.type == EnterNotify || event.type == FocusIn) {
			if ((key_grab(&window)) < 0) { goto _throw; }
		}
		else if (event.type == LeaveNotify || event.type == FocusOut) {
			key_ungrab(&window);
		}
		else if (event.type == Expose) {
			if (event.xexpose.count == 0) {
				if ((win_blit(&window)) < 0) { goto _throw; }
				if ((win_raise(&window)) < 0) { goto _throw; }
			}
		}
		else if (event.type == VisibilityNotify) {
			if (event.xvisibility.state != VisibilityUnobscured) {
				if ((win_raise(&window)) < 0) { goto _throw; }
			}
		}
		else if (event.type == KeyPress) {
			int state = 0;
			if ((state = key_press(&input, &table, &(event.xkey))) < 0) { goto _throw; }
			if (state == 0) { break; }
			if ((win_drawstate(&window, &input, &table)) < 0) { goto _throw; }
		}
		else if (event.type == NoExpose || event.type == KeyRelease) {
			continue;
		}
		else {
			eprintf("Unhandled event type: %s\n", ettoa(event.type));
		}
	}

	key_ungrab(&window);
	key_close(&input);
	win_close(&window);
	table_close(&table);
	return 0;

_throw:
	key_ungrab(&window);
	key_close(&input);
	win_close(&window);
	table_close(&table);
	return 1;
}
